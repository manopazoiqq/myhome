import { Injectable } from '@angular/core';

@Injectable()
export class Globales {  
  v_FijoVariable: string[] = ['Todo','Fijo','Variable'];
  v_IngresoEgreso: string[] = ['Todo','Ingreso','Egreso'];  
  v_SubGrupo: string[] = ['Todo','Servicio - Hogar','Servicio - Personal','Ahorro','Otros','Deuda'];
}