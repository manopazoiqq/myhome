import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class ArticulosService {  
  constructor(private afs: AngularFirestore) { }
  GetArticle(){
    return this.afs.collection('articulos',ref => ref.orderBy('StockArticle','asc')).snapshotChanges(); // articulos = es el nombre de la lista en la BD            
  }
  SaveArticle(NewArticle){     
    this.afs.collection('articulos').add(NewArticle);    
  }
  UpdateArticle(EditArticle,CodigoArticulo){
    this.afs.doc('articulos/'+CodigoArticulo).update(EditArticle);
  }
  DeleteArticle(CodigoArticulo){
    this.afs.doc('articulos/'+CodigoArticulo).delete();
  }
}
