import { TestBed } from '@angular/core/testing';

import { GastoIngresoService } from './gasto-ingreso.service';

describe('GastoIngresoService', () => {
  let service: GastoIngresoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GastoIngresoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
