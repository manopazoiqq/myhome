import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class GastoIngresoService {
  datos: string[];
  constructor(private afs: AngularFirestore) { }
  getGastoIngresoxMes(periodo){        
    return this.afs.collection('gastoingreso',ref => ref
                                                      .where('PeriodoGastoIngreso','==',periodo)                                   
                                                      ).snapshotChanges();             
  }
  insertGastIngreso(ObjectGastoIngreso){       
    this.afs.collection('gastoingreso').add(ObjectGastoIngreso);

  }
  updateGastoIngreso(ObjectGastoIngreso,IDGastoIngreso){
    this.afs.doc('gastoingreso/'+IDGastoIngreso).update(ObjectGastoIngreso);

  }
  deleteGastoIngreso(IDGastoIngreso){
    this.afs.doc('gastoingreso/'+IDGastoIngreso).delete();
  }
  getGastoIngresoDesdeHasta(desde,hasta){        
    return this.afs.collection('gastoingreso',ref => ref
                                                      .where('PeriodoGastoIngreso','>=',desde)
                                                      .where('PeriodoGastoIngreso','<=',hasta)                                   
                                                      ).snapshotChanges();             
  }
  getServicios(){
    return this.afs.collection('DescripcionGastoIngreso',ref => ref
    .where('Estado','==','V')).snapshotChanges(); 

  }
  
}
