import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class TareasService {  
  constructor(private afs: AngularFirestore) { }  
  getTasks(){
    return this.afs.collection('tarea',ref => ref.orderBy('ComplianceDateTask','asc')).snapshotChanges();             
  }
  insertTask(NewTask){
    this.afs.collection('tarea').add(NewTask);
  }
  updateTask(EditTask,IDTask){
    this.afs.doc('tarea/'+IDTask).update(EditTask);
  }
  DeleteTask(IdtASK){    
    this.afs.doc('tarea/'+IdtASK).delete();
  }  
}
