import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ArticulosService} from '../../Services/Articulos/articulos.service';
@Component({
  selector: 'app-modal-articulo',  
  templateUrl: './modal-articulo.component.html'     
})
export class ModalArticuloComponent implements OnInit{     
  // valores de entrada al componente, y estos mismos valores se los pasamos al html      
  @Input() NombreArticulo: string;
  @Input() StockArticulo: number;
  @Input() EstadoArticulo: string;  
  @Input() CodigoArticulo: string;
  seleccionado : string;
  estados:string[]=["Activo","Inactivo"]; 
  constructor(public activeModal: NgbActiveModal, private objService: ArticulosService ) { }
  ngOnInit() {
        this.seleccionado = this.EstadoArticulo;     
  }  
  saveChanges(){
    if(this.CodigoArticulo != ''){      
      this.updateArticle();
    }
    else{      
      this.insertArticle();
    }
  }
  insertArticle(){
    let NewArticle = {
      StockArticle: this.StockArticulo,
      NameArticle: this.NombreArticulo,
      StateArticle: this.EstadoArticulo
    }; // creamos un objeto...        
    this.objService.SaveArticle(NewArticle);
  }
  updateArticle(){    
    let EditArticle = {
      StockArticle: this.StockArticulo,
      NameArticle: this.NombreArticulo,
      StateArticle: this.EstadoArticulo
    }; // creamos un objeto... 
    this.objService.UpdateArticle(EditArticle,this.CodigoArticulo);
  }

}

