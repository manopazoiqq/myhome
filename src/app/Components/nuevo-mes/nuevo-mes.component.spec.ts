import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoMesComponent } from './nuevo-mes.component';

describe('NuevoMesComponent', () => {
  let component: NuevoMesComponent;
  let fixture: ComponentFixture<NuevoMesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevoMesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoMesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
