import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {GastoIngresoService} from '../../Services/GastoIngreso/gasto-ingreso.service';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-nuevo-mes',
  templateUrl: './nuevo-mes.component.html',
  styleUrls: ['./nuevo-mes.component.css']
})
export class NuevoMesComponent implements OnInit {

  constructor(public activeModal: NgbActiveModal, private objGastoIngreso: GastoIngresoService) { }
  datos: any;
  model: NgbDateStruct;
  newPeriod: NgbDateStruct;
  v_FijoVariable: string[] = ['Fijo','Variable'];
  v_IngresoEgreso: string[] = ['Ingreso','Egreso'];
  v_SubGrupo: string[] = ['Servicio - Hogar','Servicio - Personal','Ahorro','Otros','Deuda'];
  ngOnInit(): void {
  }
  getGastoIngreso(){            
    let periodo = this.model.month+'/'+this.model.year;
    this.objGastoIngreso.getGastoIngresoxMes(periodo).subscribe(data => {      
      this.datos = data.map(e => {                
       return {
         id: e.payload.doc.id,                    
         isEdit: false,         
         DescripcionGastoIngreso: e.payload.doc.data()['DescripcionGastoIngreso'],
         GrupoGastoIngreso: e.payload.doc.data()['GrupoGastoIngreso'],
         MontoGastoIngreso: e.payload.doc.data()['MontoGastoIngreso'],
         ObservacionGastoIngreso: e.payload.doc.data()['ObservacionGastoIngreso'],
         PeriodoGastoIngreso: e.payload.doc.data()['PeriodoGastoIngreso'],
         SubGrupoGastoIngreso: e.payload.doc.data()['SubGrupoGastoIngreso'],
         TipoGastoIngreso: e.payload.doc.data()['TipoGastoIngreso']         
       };
     })                     
   });    
  }
  saveChange(){   
    try {
      var periodo = this.newPeriod.month+'/'+this.newPeriod.year;            
      var existenDatos = 0;   
          this.datos.forEach(elemento => {      
            let NewObject = {
              DescripcionGastoIngreso : elemento.DescripcionGastoIngreso,
              GrupoGastoIngreso: elemento.GrupoGastoIngreso,
              MontoGastoIngreso: elemento.MontoGastoIngreso,
              ObservacionGastoIngreso: elemento.ObservacionGastoIngreso,
              PeriodoGastoIngreso: periodo,
              SubGrupoGastoIngreso: elemento.SubGrupoGastoIngreso,
              TipoGastoIngreso: elemento.TipoGastoIngreso
            }
            this.objGastoIngreso.insertGastIngreso(NewObject);
          });                
    } catch (error) {
      alert('Error al guardar cambios. Verificar datos.');
      console.error('Error linea 65 nuevo-mes.component.ts: '+error);
    }   
  }
  DeleteRowGastoIngreso(index){        
      this.datos.splice(index,1);      
  }
}
