import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TareasService} from '../../Services/Tareas/tareas.service';
@Component({
  selector: 'app-modal-tarea',
  templateUrl: './modal-tarea.component.html'  
})
export class ModalTareaComponent implements OnInit {
  // variables de entrada
  @Input() DescriptionTask: string;
  @Input() ComplianceDateTask: Date = new Date();
  @Input() MovDate: Date = new Date();
  @Input() StateTask: string;
  @Input() IDTask: string;
  estados:string[]=["Pendiente","Listo"]; 
  constructor(public activeModal: NgbActiveModal, private objServ: TareasService/*, private calendarioTareas: CalendarioTareasComponent*/) { }

  ngOnInit(): void {
  }
  SaveChanges(){    
    if(this.IDTask == ''){ 
      this.insertTask();
    }
    else{
      this.updateTask();
    }
    
  }
  insertTask(){
    let ObjectTask = {
      DescriptionTask: this.DescriptionTask,
      ComplianceDateTask: this.ComplianceDateTask,
      MovDate: this.MovDate,
      StateTask: this.StateTask,
    } 
    this.objServ.insertTask(ObjectTask);    
  }
  updateTask(){
    let ObjectTask = {
      DescriptionTask: this.DescriptionTask,
      ComplianceDateTask: this.ComplianceDateTask,
      MovDate: this.MovDate,
      StateTask: this.StateTask,
    } 
    this.objServ.updateTask(ObjectTask,this.IDTask);    
  }

}
