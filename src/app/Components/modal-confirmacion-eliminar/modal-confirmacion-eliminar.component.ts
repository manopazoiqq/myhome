import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ArticulosService } from '../../Services/Articulos/articulos.service';
import { TareasService } from '../../Services/Tareas/tareas.service';
import {GastoIngresoService} from '../../Services/GastoIngreso/gasto-ingreso.service';
@Component({
  selector: 'app-modal-confirmacion-eliminar',
  templateUrl: './modal-confirmacion-eliminar.component.html' ,
  styleUrls: ['./modal-confirmacion-eliminar.component.css']
})
export class ModalConfirmacionEliminar implements OnInit {
  @Input() CodigoArticulo: string;
  @Input() IDTask: string;
  @Input() CodigoGastoIngreso: string;
  @Input() Tipo: string; // DA = Delete Article, DT = Delete Task
  constructor(private obServ: ArticulosService, public modal: NgbActiveModal, 
              private obServTask: TareasService, private objServGI: GastoIngresoService ) { }

  ngOnInit(): void {
  }
  Delete(){
    if(this.Tipo == 'DT'){ // Eliminar tarea
      this.DeleteTask();
    }
    else if(this.Tipo == 'DA'){ // Eliminar Articulo      
        this.obServ.DeleteArticle(this.CodigoArticulo);      
    }
    else if(this.Tipo == 'DGI'){ // Eliminar Articulo      
      this.objServGI.deleteGastoIngreso(this.CodigoGastoIngreso);
    }    
  }
  DeleteArticle(){
    this.obServ.DeleteArticle(this.CodigoArticulo);
  }
  DeleteTask(){
    this.obServTask.DeleteTask(this.IDTask);
  }
}
