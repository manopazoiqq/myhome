import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalConfirmacionEliminar } from './modal-confirmacion-eliminar.component';

describe('ModalConfirmacionEliminar', () => {
  let component: ModalConfirmacionEliminar;
  let fixture: ComponentFixture<ModalConfirmacionEliminar>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalConfirmacionEliminar ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalConfirmacionEliminar);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
