import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import { GastoIngresoService } from '../../Services/GastoIngreso/gasto-ingreso.service';
import { Globales } from '../../globales';
@Component({
  selector: 'app-modal-gasto-ingreso',
  templateUrl: './modal-gasto-ingreso.component.html',
  styleUrls: ['./modal-gasto-ingreso.component.css']
})
export class ModalGastoIngresoComponent implements OnInit {  
  @Input() CodigoGastoIngreso: string;
  @Input() DescripcionGastoIngreso:string;
  @Input() FijoVariable:string;
  @Input() IngresoEgreso:string;
  @Input() SubGrupoGastoIngreso: string;
  @Input() MontoGastoIngreso: number;
  @Input() ObservacionGastoIngreso: string;
  @Input() periodo: object;
  PeriodoGastoIngreso: NgbDateStruct;    
  v_FijoVariable: string[]=['Fijo','Variable'];
  v_IngresoEgreso: string[]= ['Ingreso','Egreso'];  
  v_SubGrupo: string[]= ['Servicio - Hogar','Servicio - Personal','Ahorro','Otros','Deuda'];
  constructor(public activeModal: NgbActiveModal, private obService: GastoIngresoService, private v_globales: Globales) { }    
  ngOnInit(): void {        
    this.PeriodoGastoIngreso = {
      month: parseInt(this.periodo['month']),
      year: parseInt(this.periodo['year']),
      day: 1
    };        
  }
  saveChange(){    
    if(this.CodigoGastoIngreso == ''){this.InsertGastoIngreso()}
    else if(this.CodigoGastoIngreso != ''){this.UpdateGastoIngreso()}
  }
  InsertGastoIngreso(){
    let periodo = this.PeriodoGastoIngreso.month+'/'+this.PeriodoGastoIngreso.year;
    this.MontoGastoIngreso = (this.IngresoEgreso == 'Egreso' && this.MontoGastoIngreso > 0) ? (this.MontoGastoIngreso *-1) : this.MontoGastoIngreso;
    let ObjectGastoIngreso = {
      DescripcionGastoIngreso : this.DescripcionGastoIngreso,
      GrupoGastoIngreso: this.IngresoEgreso,
      MontoGastoIngreso: this.MontoGastoIngreso,
      ObservacionGastoIngreso: this.ObservacionGastoIngreso,
      PeriodoGastoIngreso: periodo,
      SubGrupoGastoIngreso: this.SubGrupoGastoIngreso,
      TipoGastoIngreso: this.FijoVariable
    }
    this.obService.insertGastIngreso(ObjectGastoIngreso);
  }
  UpdateGastoIngreso(){
    let periodo = this.PeriodoGastoIngreso.month+'/'+this.PeriodoGastoIngreso.year;
    this.MontoGastoIngreso = (this.IngresoEgreso == 'Egreso' && this.MontoGastoIngreso > 0) ? (this.MontoGastoIngreso *-1) : this.MontoGastoIngreso;
    let ObjectGastoIngreso = {
      DescripcionGastoIngreso : this.DescripcionGastoIngreso,
      GrupoGastoIngreso: this.IngresoEgreso,
      MontoGastoIngreso: this.MontoGastoIngreso,
      ObservacionGastoIngreso: this.ObservacionGastoIngreso,
      PeriodoGastoIngreso: periodo,
      SubGrupoGastoIngreso: this.SubGrupoGastoIngreso,
      TipoGastoIngreso: this.FijoVariable
    }
    this.obService.updateGastoIngreso(ObjectGastoIngreso,this.CodigoGastoIngreso);    
  }

}
