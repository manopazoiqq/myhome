import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalGastoIngresoComponent } from './modal-gasto-ingreso.component';

describe('ModalGastoIngresoComponent', () => {
  let component: ModalGastoIngresoComponent;
  let fixture: ComponentFixture<ModalGastoIngresoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalGastoIngresoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalGastoIngresoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
