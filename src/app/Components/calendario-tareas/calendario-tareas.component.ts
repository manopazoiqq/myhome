import { Component, OnInit } from '@angular/core';
import {TareasService} from '../../Services/Tareas/tareas.service';
import {NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {ModalTareaComponent} from '../modal-tarea/modal-tarea.component';
import {ModalConfirmacionEliminar} from '../modal-confirmacion-eliminar/modal-confirmacion-eliminar.component';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-calendario-tareas',
  templateUrl: './calendario-tareas.component.html'  
})
export class CalendarioTareasComponent implements OnInit {
  datos : any[];
  datosOriginal : any[];  
  selectFiltroCalendario:string = 'Todo';
  constructor(private servicio: TareasService, 
              private ActivarModal: NgbModal,private spinner: NgxSpinnerService) {}
  ngOnInit(): void {    
    this.spinner.show();
    this.servicio.getTasks().subscribe(data => {      
       this.datos = data.map(e => {         
        return {
          id: e.payload.doc.id,                    
          isEdit: false,
          IDTask: e.payload.doc.id,
          DescriptionTask: e.payload.doc.data()['DescriptionTask'],
          ComplianceDateTask: e.payload.doc.data()['ComplianceDateTask'],
          StateTask: e.payload.doc.data()['StateTask'],
          MovDate: e.payload.doc.data()['MovDate']      
        };
      })      
      this.spinner.hide(); 
      this.datosOriginal = this.datos; 
    });
  }
  insertTask(){
    this.showModal('','','','','');
  }
  updateTask(DescriptionTask,ComplianceDateTask,MovDate,StateTask,IDTask){
    this.showModal(DescriptionTask,ComplianceDateTask,MovDate,StateTask,IDTask);
  }
  deleteTask(IDTask){
    const modalRef = this.ActivarModal.open(ModalConfirmacionEliminar);
    modalRef.componentInstance.IDTask = IDTask; // pasamos valores como output    
    modalRef.componentInstance.Tipo = 'DT';
  }  
  changeStateTask(DescriptionTask,ComplianceDateTask,MovDate,IDTask){ // dejar como "LISTA" una tarea
  let ObjectTask = {
    DescriptionTask: DescriptionTask,
    ComplianceDateTask: ComplianceDateTask,
    MovDate: MovDate,
    StateTask: 'Listo',
  }
    this.servicio.updateTask(ObjectTask,IDTask);
  }
  showModal(DescriptionTask:string = '',ComplianceDateTask ,MovDate,StateTask: string = '',IDTask: string){ // se reciben parametros inicializados en 0 o por defecto vacío        
    const modalRef = this.ActivarModal.open(ModalTareaComponent); // levantamos modal
    modalRef.componentInstance.IDTask = IDTask; // pasamos valores como output
    modalRef.componentInstance.DescriptionTask = DescriptionTask; // pasamos valores como output
    modalRef.componentInstance.ComplianceDateTask = ComplianceDateTask; // pasamos valores como output
    modalRef.componentInstance.MovDate = MovDate; // pasamos valores como output
    modalRef.componentInstance.StateTask = StateTask; // pasamos valores como output    
  }
  taskFilter(){    
    const newV = this.datosOriginal.filter((objeto) => objeto.StateTask == (this.selectFiltroCalendario == 'Todo' ? objeto.StateTask : this.selectFiltroCalendario) );    
    this.datos = newV;
  }
}
