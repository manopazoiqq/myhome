import { Component } from '@angular/core';
import { ArticulosService } from '../../Services/Articulos/articulos.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalArticuloComponent } from '../modal-articulo/modal-articulo.component';
import { ModalConfirmacionEliminar } from '../modal-confirmacion-eliminar/modal-confirmacion-eliminar.component';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-mantenedor-articulos',
  templateUrl: './mantenedor-articulos.component.html',
  styleUrls: ['./mantenedor-articulos.component.css']
})
export class MantenedorArticulosComponent {
  datos: any = [];
  buscarArticulo: string = '';
  constructor(private ServicioArticulo: ArticulosService, private ActivarModal: NgbModal,
    private spinner: NgxSpinnerService) { }
  ngOnInit() {
    this.spinner.show();
    this.ServicioArticulo.GetArticle().subscribe(data => {
      this.datos = data.map(e => {
        return {
          id: e.payload.doc.id,
          isEdit: false,
          ID: e.payload.doc.data()['ID'],
          NameArticle: e.payload.doc.data()['NameArticle'],
          StateArticle: e.payload.doc.data()['StateArticle'],
          StockArticle: e.payload.doc.data()['StockArticle'],
        };
      })
      this.spinner.hide();
    });
  }
  deleteArticle(CodigoArticle) {
    const modalRef = this.ActivarModal.open(ModalConfirmacionEliminar);
    modalRef.componentInstance.CodigoArticulo = CodigoArticle; // pasamos valores como output    
    modalRef.componentInstance.Tipo = 'DA';
  }
  insertArticle() {
    this.showModal('', '', 0, 'Activo');
  }
  updateArticle(codigoArticulo: string = '', nombreArticulo: string, stockArticulo: number, estadoArticulo: string) {
    this.showModal(codigoArticulo, nombreArticulo, stockArticulo, estadoArticulo);
  }
  showModal(codigoArticulo: string = '', nombreArticulo: string = '', stockArticulo: number = 0, estadoArticulo: string = '') { // se reciben parametros inicializados en 0 o por defecto vacío    
    const modalRef = this.ActivarModal.open(ModalArticuloComponent); // levantamos modal
    modalRef.componentInstance.CodigoArticulo = codigoArticulo; // pasamos valores como output
    modalRef.componentInstance.NombreArticulo = nombreArticulo; // pasamos valores como output
    modalRef.componentInstance.StockArticulo = stockArticulo; // pasamos valores como output
    modalRef.componentInstance.EstadoArticulo = estadoArticulo; // pasamos valores como output    
  }
}
