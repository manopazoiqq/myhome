import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MantenedorArticulosComponent } from './mantenedor-articulos.component';

describe('MantenedorArticulosComponent', () => {
  let component: MantenedorArticulosComponent;
  let fixture: ComponentFixture<MantenedorArticulosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MantenedorArticulosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MantenedorArticulosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
