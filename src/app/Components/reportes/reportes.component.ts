import { Component, OnInit } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Label } from 'ng2-charts';
import { NgbDate, NgbCalendar, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { Globales } from '../../globales';
import { GastoIngresoService } from '../../Services/GastoIngreso/gasto-ingreso.service';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-reportes',
  templateUrl: './reportes.component.html',
  styles: [`
    .form-group.hidden {
      width: 0;
      margin: 0;
      border: none;
      padding: 0;
    }
    .custom-day {
      text-align: center;
      padding: 0.185rem 0.25rem;
      display: inline-block;
      height: 2rem;
      width: 2rem;
    }
    .custom-day.focused {
      background-color: #e6e6e6;
    }
    .custom-day.range, .custom-day:hover {
      background-color: rgb(2, 117, 216);
      color: white;
    }
    .custom-day.faded {
      background-color: rgba(2, 117, 216, 0.5);
    }
    button.calendar,
    button.calendar:active {
    width:2.75rem;
    background-image:url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAcCAYAAAAEN20fAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAOxAAADsQBlSsOGwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAEUSURBVEiJ7ZQxToVAEIY/YCHGxN6XGOIpnpaEsBSeQC9ArZbm9TZ6ADyBNzAhQGGl8Riv4BLAWAgmkpBYkH1b8FWT2WK/zJ8ZJ4qiI6XUI3ANnGKWBnht2/ZBDRK3hgVGNsCd7/ui+JkEIrKtqurLpEWaphd933+IyI3LEIdpCYCiKD6HcuOa/nwOa0ScJEnk0BJg0UTUWJRl6RxCYEzEmomsIlPU3IPW+grIAbquy+q6fluy/28RIBeRMwDXdXMgXLj/B2uimRXpui4D9sBeRLKl+1N+L+t6RwbWrZliTTTr1oxYtzVWiTQAcRxvTX+eJMnlUDaO1vpZRO5NS0x48sIwfPc87xg4B04MCzQi8hIEwe4bl1DnFMCN2zsAAAAASUVORK5CYII=")!important;
    background-repeat:no-repeat;
    background-size:23px;
    background-position:50%
    }
  `]
})
export class ReportesComponent implements OnInit {
  mostrarGrafico: boolean = false;
  hoveredDate: NgbDate | null = null;
  fromDate: NgbDate | null;
  toDate: NgbDate | null;
  IngresoEgreso: string = 'Todo';  
  v_IngresoEgreso: string[];
  v_SubGrupo: string[];
  serviciosBD: any;
  servicioGastoIngreso: string = 'Todo'
  SubGrupo: string = 'Todo';
  datos: any;
  datosOriginal: any;
  servicios: string[] = [];
  periodos: string[] = ['']; // array de string (01/2020, 02/2020, etc...)
  colores: string[] = ['#007bff','#6c757d','#28a745','#dc3545','#ffc107','#17a2b8']; // colores de bootstrap xD
  public barChartOptions: ChartOptions = {
    responsive: true,
  };
  public barChartLabels: Label[] = this.periodos;
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [];

  public barChartData: ChartDataSets[] = [];
  constructor(private v_globales: Globales, private calendar: NgbCalendar, public formatter: NgbDateParserFormatter, private objGastoIngreso: GastoIngresoService,private spinner: NgxSpinnerService) {
    this.fromDate = calendar.getToday();
    this.toDate = calendar.getNext(calendar.getToday(), 'd', 10);
  }
  ngOnInit() {    
    this.v_IngresoEgreso = this.v_globales.v_IngresoEgreso;
    this.v_SubGrupo = this.v_globales.v_SubGrupo;
    this.obtenerTodosGastoIngreso();
  }
  generarReporte() {    
    this.spinner.show()
    this.barChartData = []
    this.barChartLabels = []
    this.armarPeriodos();
    let fechaDesde: string = this.fromDate.month + '/' + this.fromDate.year
    let fechaHasta: string = this.toDate.month + '/' + this.toDate.year    
    this.barChartLabels = this.periodos;    
    this.objGastoIngreso.getGastoIngresoDesdeHasta(fechaDesde, fechaHasta).subscribe(data => {
      this.datos = data.map(e => {
        return {
          id: e.payload.doc.id,
          isEdit: false,
          IDGastoIngreso: e.payload.doc.id,
          DescripcionGastoIngreso: e.payload.doc.data()['DescripcionGastoIngreso'],
          GrupoGastoIngreso: e.payload.doc.data()['GrupoGastoIngreso'],
          MontoGastoIngreso: e.payload.doc.data()['MontoGastoIngreso'],
          ObservacionGastoIngreso: e.payload.doc.data()['ObservacionGastoIngreso'],
          PeriodoGastoIngreso: e.payload.doc.data()['PeriodoGastoIngreso'],
          SubGrupoGastoIngreso: e.payload.doc.data()['SubGrupoGastoIngreso'],
          TipoGastoIngreso: e.payload.doc.data()['TipoGastoIngreso']
        };        
      })
      this.datosOriginal = this.datos;
      this.filtraGastoIngreso(); // funcion que filtra los datos segun los parametros seleccionados      
      this.obtenerServiciosArray();                        
      this.servicios.forEach(servicio => {        
        var valores = [];
        for (var k = 0; k < this.datos.length; k++) {          
          if(servicio == this.datos[k].DescripcionGastoIngreso){
            let MontoGastoIngreso = (this.datos[k].GrupoGastoIngreso == 'Egreso') ? (this.datos[k].MontoGastoIngreso * -1) : this.datos[k].MontoGastoIngreso
            valores.push(MontoGastoIngreso);
          }
        }
        let color = this.colorHEX()
        let objetoNuevo = {
          label : servicio,
          data : valores,
          backgroundColor: color 
        }
        /************** Estructura ejemplo de como se arma el objeto para el gráfico
        { data: [20000, 11000,12000], label: 'Agua' },
        { data: [20000, 11000,12000], label: 'Luz' },
        { data: [30000, 41000,25000], label: 'Gas' }
        *****************************************************************/
        this.barChartData.push(objetoNuevo);      
        this.spinner.hide()  
      });            
      this.mostrarGrafico = this.datos.length > 0 ? true : false
    });                  
  }  
  obtenerServiciosArray(){    
    // funcion que llena el array servicios con los distintos servicios encontrado en this.datos
    this.servicios = [];
    this.datos.forEach(elemento => {      
      let yaExiste = this.servicios.findIndex(function(servicio){
        return servicio == elemento.DescripcionGastoIngreso;
      });
      if(yaExiste == -1){ // No existe el servicio, debemos ingresarlo al array
        this.servicios.push(elemento.DescripcionGastoIngreso);
      }      
    });            
  }
  armarPeriodos(){
    /****** rutina para obtener meses entre el periodo y meterlos a un array *************/
    let mesDesde: number = this.fromDate.month;
    let anhoDesde: number = this.fromDate.year;
    let mesHasta: number = this.toDate.month;
    let anhoHasta: number = this.toDate.year;    
    this.periodos = [];    
    if (this.fromDate.year == this.toDate.year) { // si son del mismo año, solo restamos los meses
      let mesesDif = mesHasta - mesDesde;
      for (let i = 0; i <= mesesDif; i++) {
        let nuevoMes = (mesDesde + i) + '/' + anhoDesde;
        this.periodos.push(nuevoMes);
      }
    }
    else { // si el periodo es por más de un año...
      let anhoDif = anhoHasta - anhoDesde;
      for (let i = 0; i <= anhoDif; i++) {
        if (i == 0) { // primer año
          let mesesDif = 12 - mesDesde;
          for (let i = 0; i <= mesesDif; i++) {
            let nuevoMes = (mesDesde + i) + '/' + anhoDesde;
            this.periodos.push(nuevoMes);
          }
        }
        else{
          let anhoActualProceso = anhoDesde + i;            
          let mesActualProceso= 0;
          if(anhoHasta == anhoActualProceso){ // llegamos al último año            
            for (let i = 1; i <= mesHasta; i++) {              
              let nuevoMes = (mesActualProceso + i) + '/' + anhoHasta;
              this.periodos.push(nuevoMes);
            }
          }
          else{            
            for (let i = 1; i <= 12; i++) {
              let nuevoMes = (mesActualProceso + i) + '/' + anhoActualProceso;
              this.periodos.push(nuevoMes);
            }
          }
        }
      }
    }     
  }
  filtraGastoIngreso(){      // filtro sobre filtro sobre filtro               
    const nuevoDatos = this.datos.    
    filter((objeto)=>objeto.GrupoGastoIngreso == (this.IngresoEgreso == 'Todo' ? objeto.GrupoGastoIngreso: this.IngresoEgreso) ).
    filter((objeto)=>objeto.SubGrupoGastoIngreso == (this.SubGrupo == 'Todo' ? objeto.SubGrupoGastoIngreso: this.SubGrupo) ).
    filter((objeto)=>objeto.DescripcionGastoIngreso == (this.servicioGastoIngreso == 'Todo' ? objeto.DescripcionGastoIngreso: this.servicioGastoIngreso) )
    ;
    this.datos = nuevoDatos;        
  }
  obtenerTodosGastoIngreso(){
    this.objGastoIngreso.getServicios().subscribe(data => {
      this.serviciosBD = data.map(e => {
        return {
          id: e.payload.doc.id,
          isEdit: false,
          IDServicio: e.payload.doc.id,
          nombreServicio: e.payload.doc.data()['Nombre'],          
          tipoServicio: e.payload.doc.data()['IngresoEgreso']
        };        
      })                  
    });
  }
  generarLetra(){
    let letras = ["a","b","c","d","e","f","0","1","2","3","4","5","6","7","8","9"];
    let numero = (Math.random()*15).toFixed(0);
    return letras[numero];
  }
  colorHEX(){
    let color = "";
    for(let i=0;i<6;i++){
      color = color + this.generarLetra() ;
    }
    return "#" + color;
  }
  // funciones del calendario...
  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date && date.after(this.fromDate)) {
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }
  isHovered(date: NgbDate) { return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate); }

  isInside(date: NgbDate) { return this.toDate && date.after(this.fromDate) && date.before(this.toDate);}

  isRange(date: NgbDate) { return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date); }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  } 
}


