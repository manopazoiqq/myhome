import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GastosHogarComponent } from './gastos-hogar.component';

describe('GastosHogarComponent', () => {
  let component: GastosHogarComponent;
  let fixture: ComponentFixture<GastosHogarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GastosHogarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GastosHogarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
