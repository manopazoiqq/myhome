import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { GastoIngresoService } from '../../Services/GastoIngreso/gasto-ingreso.service';
import { ModalGastoIngresoComponent } from '../modal-gasto-ingreso/modal-gasto-ingreso.component';
import {NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {ModalConfirmacionEliminar} from '../modal-confirmacion-eliminar/modal-confirmacion-eliminar.component';
import {NuevoMesComponent} from '../../Components/nuevo-mes/nuevo-mes.component';
import { Globales } from '../../globales';
@Component({
  selector: 'app-gastos-hogar',
  templateUrl: './gastos-hogar.component.html',
  styleUrls: ['./gastos-hogar.component.css']
})
export class GastosHogarComponent implements OnInit {
  datos: any;
  datosOriginal: any;
  model: NgbDateStruct;
  IngresoEgreso: string = 'Todo';
  FijoVariable: string = 'Todo';
  v_FijoVariable: string[];
  v_IngresoEgreso: string[];
  v_SubGrupo: string[];
  SubGrupo: string = 'Todo';
  periodo: object;
  mes: number;
  anho: number;
  sumaTotalGastoIngreso: number = 0;
  //@Output() CambioTotalGastoIngreso = new EventEmitter<any>();
  constructor(private objGastoIngreso: GastoIngresoService,private ActivarModal: NgbModal, private v_globales: Globales) { }  
  ngOnInit(): void {
    this.v_FijoVariable = this.v_globales.v_FijoVariable;
    this.v_IngresoEgreso = this.v_globales.v_IngresoEgreso;
    this.v_SubGrupo = this.v_globales.v_SubGrupo; 
    let fec_actual = new Date();    
    this.model = {
      year: fec_actual.getFullYear(),
      month: fec_actual.getMonth()+1,
      day: fec_actual.getDate()
    }           
    this.getGastoIngreso();
  }
  insertGastoIngreso(){
    this.showModal('','','Fijo','Ingreso','Servicio Basico','','','');
  }
  updateGastoIngreso(CodigoGastoIngreso,DescripcionGastoIngreso,FijoVariable,IngresoEgreso,SubGrupoGastoIngreso,
    MontoGastoIngreso,ObservacionGastoIngreso,PeriodoGastoIngreso){    
      this.mes = PeriodoGastoIngreso.split('/')[0];
      this.anho = PeriodoGastoIngreso.split('/')[1];
      this.periodo = {year: this.anho, month: this.mes, day: 1}            
    this.showModal(CodigoGastoIngreso,DescripcionGastoIngreso,FijoVariable,IngresoEgreso,SubGrupoGastoIngreso,
      MontoGastoIngreso,ObservacionGastoIngreso,this.periodo);
  }
  deleteGastoIngreso(CodigoGastoIngreso){
    const modalRef = this.ActivarModal.open(ModalConfirmacionEliminar);
    modalRef.componentInstance.CodigoGastoIngreso = CodigoGastoIngreso; // pasamos valores como output    
    modalRef.componentInstance.Tipo = 'DGI';

  }
  showModal(CodigoGastoIngreso,DescripcionGastoIngreso,FijoVariable,IngresoEgreso,SubGrupoGastoIngreso,
                MontoGastoIngreso,ObservacionGastoIngreso,PeriodoGastoIngreso){    
    const modalRef = this.ActivarModal.open(ModalGastoIngresoComponent); // levantamos modal
    modalRef.componentInstance.CodigoGastoIngreso = CodigoGastoIngreso; // pasamos valores como output
    modalRef.componentInstance.DescripcionGastoIngreso = DescripcionGastoIngreso; // pasamos valores como output
    modalRef.componentInstance.FijoVariable = FijoVariable; // pasamos valores como output
    modalRef.componentInstance.IngresoEgreso = IngresoEgreso; // pasamos valores como output
    modalRef.componentInstance.SubGrupoGastoIngreso = SubGrupoGastoIngreso; // pasamos valores como output
    modalRef.componentInstance.MontoGastoIngreso = MontoGastoIngreso; // pasamos valores como output
    modalRef.componentInstance.ObservacionGastoIngreso = ObservacionGastoIngreso; // pasamos valores como output
    modalRef.componentInstance.periodo = PeriodoGastoIngreso; // pasamos valores como output
  }
  getGastoIngreso(){            
    let periodo = this.model.month+'/'+this.model.year;
    this.objGastoIngreso.getGastoIngresoxMes(periodo).subscribe(data => {      
      this.datos = data.map(e => {                
       return {
         id: e.payload.doc.id,                    
         isEdit: false,
         IDGastoIngreso: e.payload.doc.id,
         DescripcionGastoIngreso: e.payload.doc.data()['DescripcionGastoIngreso'],
         GrupoGastoIngreso: e.payload.doc.data()['GrupoGastoIngreso'],
         MontoGastoIngreso: e.payload.doc.data()['MontoGastoIngreso'],
         ObservacionGastoIngreso: e.payload.doc.data()['ObservacionGastoIngreso'],
         PeriodoGastoIngreso: e.payload.doc.data()['PeriodoGastoIngreso'],
         SubGrupoGastoIngreso: e.payload.doc.data()['SubGrupoGastoIngreso'],
         TipoGastoIngreso: e.payload.doc.data()['TipoGastoIngreso']         
       };
     })           
     //this.spinner.hide();  
     this.datosOriginal = this.datos;
     this.filterGastoIngreso();
     this.getSumaTotalGastoIngreso();
   });    
  }
  filterGastoIngreso(){      // filtro sobre filtro sobre filtro       
    const nuevoDatos = this.datosOriginal.
    filter((objeto)=>objeto.GrupoGastoIngreso == (this.IngresoEgreso == 'Todo' ? objeto.GrupoGastoIngreso: this.IngresoEgreso) ).
    filter((objeto)=>objeto.TipoGastoIngreso == (this.FijoVariable == 'Todo' ? objeto.TipoGastoIngreso : this.FijoVariable)).
    filter((objeto)=>objeto.SubGrupoGastoIngreso == (this.SubGrupo == 'Todo' ? objeto.SubGrupoGastoIngreso: this.SubGrupo) );
    this.datos = nuevoDatos;    
    this.getSumaTotalGastoIngreso();
  }
  getSumaTotalGastoIngreso(){    
    this.sumaTotalGastoIngreso = this.datos.reduce((total,item)=>{     
      return total + item.MontoGastoIngreso;
    },0 );        
  }
  showModalNewMes(){
    const modalRef = this.ActivarModal.open(NuevoMesComponent,{ size: 'xl' }); // levantamos modal    
  }
}
