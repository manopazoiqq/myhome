import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'filtro'
})
export class FiltroPipe implements PipeTransform {
  transform(value: any, arg: any): any {
    // value = array...
    // arg = valor a buscar en el array...
    const Resultado = [];
    if(typeof arg === 'undefined' || arg=== '' || arg.length < 2 ) return value;
    for(const fila of value){ // recorremos todo el value pasado, serìa el array...
      if(fila.NameArticle.toLowerCase().indexOf(arg.toLowerCase()) > -1){  
      // Si encuentra el indexOf del valor que se está buscando, lo agrega al array para devolver...       
        Resultado.push(fila);
      }
    }
    return Resultado;
  }
}

