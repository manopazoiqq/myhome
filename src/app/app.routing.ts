import { RouterModule, Routes } from '@angular/router';
import  {MantenedorArticulosComponent } from './Components/mantenedor-articulos/mantenedor-articulos.component';
import {CalendarioTareasComponent} from './Components/calendario-tareas/calendario-tareas.component';
import { GastosHogarComponent } from './Components/gastos-hogar/gastos-hogar.component';
import {ReportesComponent} from './Components/reportes/reportes.component';

const appRoutes = [
  { path: '',   redirectTo: '/mantenedor-articulos', pathMatch: 'full' } , // se va a esta ruta por defecto...  
  { path: 'mantenedor-articulos', component: MantenedorArticulosComponent},
  { path: 'calendario-tareas', component: CalendarioTareasComponent},
  { path: 'gastos-hogar', component: GastosHogarComponent},
  { path: 'reportes', component: ReportesComponent}
          
  ];
  export const routing = RouterModule.forRoot(appRoutes);