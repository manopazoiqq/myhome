import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { routing } from './app.routing';

import { AppComponent } from './app.component';
// Firebase
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../environments/environment';
// ngx spinner
import { NgxSpinnerModule } from "ngx-spinner";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Componentes
import { MantenedorArticulosComponent } from './Components/mantenedor-articulos/mantenedor-articulos.component';
import { BarraNavegacionComponent } from './Components/barra-navegacion/barra-navegacion.component';
import { TareasComponent } from './Components/tareas/tareas.component';
import { CalendarioTareasComponent } from './Components/calendario-tareas/calendario-tareas.component';
import { ModalArticuloComponent } from './Components/modal-articulo/modal-articulo.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalConfirmacionEliminar } from './Components/modal-confirmacion-eliminar/modal-confirmacion-eliminar.component';
import { FiltroPipe } from './Pipes/filtro.pipe';
import { GastosHogarComponent } from './Components/gastos-hogar/gastos-hogar.component';
import { ModalTareaComponent } from './Components/modal-tarea/modal-tarea.component';
import { ModalGastoIngresoComponent } from './Components/modal-gasto-ingreso/modal-gasto-ingreso.component';
import { NuevoMesComponent } from './Components/nuevo-mes/nuevo-mes.component';
import { ChartsModule } from 'ng2-charts';
import { ReportesComponent } from './Components/reportes/reportes.component';
import { Globales } from './globales';

@NgModule({
  declarations: [
    AppComponent,
    MantenedorArticulosComponent,
    BarraNavegacionComponent,
    TareasComponent,
    CalendarioTareasComponent,
    ModalArticuloComponent,
    ModalConfirmacionEliminar,
    FiltroPipe,
    GastosHogarComponent,
    ModalTareaComponent,
    ModalGastoIngresoComponent,
    NuevoMesComponent,
    ReportesComponent        
  ],
  imports: [
    BrowserModule,
    routing,
    NgbModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),    
    AngularFirestoreModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    ChartsModule
  ],
  providers: [Globales],
  bootstrap: [AppComponent]
})
export class AppModule { }
