// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyD6JB5yTjVv-Luull3gvHtTeSME1bCv0lM",
    authDomain: "myhome-647ce.firebaseapp.com",
    databaseURL: "https://myhome-647ce.firebaseio.com",
    projectId: "myhome-647ce",
    storageBucket: "myhome-647ce.appspot.com",
    messagingSenderId: "412821838208",
    appId: "1:412821838208:web:46b90baa60d02862b24965",
    measurementId: "G-6WBN27FEDV"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
